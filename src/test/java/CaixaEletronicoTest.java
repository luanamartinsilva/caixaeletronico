import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Before;
import org.junit.Test;

public class CaixaEletronicoTest {
	
	private static final String numeroConta = "123456";
	private HardwareMock hardwareMock;
	private ServicoRemotoMock servicoRemotoMock;
	
	@Before
	public void setUp() {
		hardwareMock = new HardwareMock();
		hardwareMock.setNumeroContaCartao(numeroConta);
		servicoRemotoMock = new ServicoRemotoMock();
		ContaCorrente conta = new ContaCorrente(50.0);
		servicoRemotoMock.adicionarConta(numeroConta , conta);
	}

	@Test
	public void testarLogarComSucesso() {

		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, null);
		
		assertEquals("Usuário Autenticado", caixaEletronico.logar());
		assertEquals("pegarNumeroDaContaCartao", hardwareMock.getServicoChamado());

	}

	@Test
	public void testarLogarComFalha() {

		hardwareMock.setLancarExcecaoPegarNumero(true);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, null);
		
		assertEquals("Não foi possível autenticar o usuário", caixaEletronico.logar());
		assertEquals("pegarNumeroDaContaCartao", hardwareMock.getServicoChamado());

	}

	@Test
	public void testarSaldoComSucesso() {
		
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("O saldo é R$50.00", caixaEletronico.saldo());
		assertEquals("recuperarConta", servicoRemotoMock.getServicoChamado());

	}
	
	@Test
	public void testarDepositarComSucesso() {
		
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("Depósito recebido com sucesso", caixaEletronico.depositar(50.0));
		assertEquals("persistirConta", servicoRemotoMock.getServicoChamado());
		assertEquals("lerEnvelope", hardwareMock.getServicoChamado());
		assertEquals(new Double(100.0), servicoRemotoMock.getConta(numeroConta).getSaldo());

	}
	
	@Test
	public void testarDepositarComFalha() {

		hardwareMock.setLancarExcecaoLerEnvelope(true);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("Falha ao realizar depósito", caixaEletronico.depositar(50.0));
		assertNotSame("persistirConta", servicoRemotoMock.getServicoChamado());
		assertEquals("lerEnvelope", hardwareMock.getServicoChamado());
		assertEquals(new Double(50.0), servicoRemotoMock.getConta(numeroConta).getSaldo());

	}
	
	@Test
	public void testarSacarComSucesso() {

		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("Retire seu dinheiro", caixaEletronico.sacar(30.0));
		assertEquals("persistirConta", servicoRemotoMock.getServicoChamado());
		assertEquals("entregarDinheiro", hardwareMock.getServicoChamado());
		assertEquals(new Double(20.0), servicoRemotoMock.getConta(numeroConta).getSaldo());

	}
	
	@Test
	public void testarSacarComFalha() {

		hardwareMock.setLancarExcecaoEntregarDinheiro(true);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("Falha ao realizar o saque", caixaEletronico.sacar(30.0));
		assertEquals("entregarDinheiro", hardwareMock.getServicoChamado());
		assertNotSame("persistirConta", servicoRemotoMock.getServicoChamado());
		assertEquals(new Double(50.0), servicoRemotoMock.getConta(numeroConta).getSaldo());

	}
	
	@Test
	public void testarSacarSaldoInsuficiente() {
		
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardwareMock, servicoRemotoMock);
		caixaEletronico.logar();
		
		assertEquals("Saldo Insuficiente", caixaEletronico.sacar(60.0));
		assertNotSame("entregarDinheiro", hardwareMock.getServicoChamado());
		assertNotSame("persistirConta", servicoRemotoMock.getServicoChamado());
		assertEquals(new Double(50.0), servicoRemotoMock.getConta(numeroConta).getSaldo());

	}

}
