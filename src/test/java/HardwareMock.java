
public class HardwareMock implements Hardware {

	private String servicoChamado;
	private boolean lancarExcecaoPegarNumero = false;
	private boolean lancarExcecaoEntregarDinheiro = false;
	private boolean lancarExcecaoLerEnvelope = false;
	private String numeroContaCartao;

	@Override
	public String pegarNumeroDaContaCartao() {
		this.servicoChamado = "pegarNumeroDaContaCartao";
		if (this.lancarExcecaoPegarNumero) {
			throw new RuntimeException();
		}
		return this.numeroContaCartao;
	}

	@Override
	public void lerEnvelope() {
		this.servicoChamado = "lerEnvelope";
		if (this.lancarExcecaoLerEnvelope) {
			throw new RuntimeException();
		}
	}
	
	@Override
	public void entregarDinheiro() {
		this.servicoChamado = "entregarDinheiro";
		if (this.lancarExcecaoEntregarDinheiro) {
			throw new RuntimeException();
		}		
	}
	
//	Métodos de teste
	
	public String getServicoChamado() {
		return this.servicoChamado;
	}

	public void setNumeroContaCartao(String numeroConta) {
		this.numeroContaCartao = numeroConta;
	}

	public void setLancarExcecaoPegarNumero(boolean lancarExcecaoPegarNumero) {
		this.lancarExcecaoPegarNumero = lancarExcecaoPegarNumero;
	}

	public void setLancarExcecaoEntregarDinheiro(boolean lancarExcecaoEntregarDinheiro) {
		this.lancarExcecaoEntregarDinheiro = lancarExcecaoEntregarDinheiro;
	}

	public void setLancarExcecaoLerEnvelope(boolean lancarExcecaoLerEnvelope) {
		this.lancarExcecaoLerEnvelope = lancarExcecaoLerEnvelope;
	}
	
	


}
