import java.util.HashMap;
import java.util.Map;

public class ServicoRemotoMock implements ServicoRemoto {

	private String servicoChamado;
	private Map<String, ContaCorrente> contas = new HashMap<>();

	@Override
	public ContaCorrente recuperarConta(String numero) {
		this.servicoChamado = "recuperarConta";
		return (ContaCorrente) this.contas.get(numero).clone();
	}
	
	@Override
	public void persistirConta(ContaCorrente conta, String numeroConta) {
		this.servicoChamado = "persistirConta";
		this.contas.put(numeroConta, conta);	
	}
	
//	Métodos de teste
	
	public String getServicoChamado() {
		return this.servicoChamado;
	}

	public void adicionarConta(String numeroConta, ContaCorrente conta) {
		this.contas.put(numeroConta, conta);
		
	}

	public ContaCorrente getConta(String numeroConta) {
		return this.contas.get(numeroConta);
	}

}
