
public class CaixaEletronico {
	
	private Hardware hardware;
	private ServicoRemoto servicoRemoto;
	private String numeroConta;

	public CaixaEletronico(Hardware hardware, ServicoRemoto servicoRemoto) {
		this.hardware = hardware;
		this.servicoRemoto = servicoRemoto;
	}

	public String logar() {
		try {
			this.numeroConta = hardware.pegarNumeroDaContaCartao();
		} catch (Exception e) {
			return "Não foi possível autenticar o usuário";
		}
		return "Usuário Autenticado";
	}

	public String saldo() {
		
		ContaCorrente conta = this.servicoRemoto.recuperarConta(numeroConta);
		return String.format("O saldo é R$%.2f", conta.getSaldo());
	}

	public String depositar(Double valor) {
		try {
			this.hardware.lerEnvelope();
		} catch (Exception e) {
			return "Falha ao realizar depósito";
		}
		ContaCorrente conta = this.servicoRemoto.recuperarConta(numeroConta);
		conta.setSaldo(conta.getSaldo() + valor);
		this.servicoRemoto.persistirConta(conta, this.numeroConta);
		return "Depósito recebido com sucesso";
	}

	public String sacar(Double valor) {
		ContaCorrente conta = this.servicoRemoto.recuperarConta(numeroConta);
		conta.setSaldo(conta.getSaldo() - valor);
		if (conta.getSaldo() < 0) {
			return "Saldo Insuficiente";
		}
		try {
			this.hardware.entregarDinheiro();
		} catch (Exception e) {
			return "Falha ao realizar o saque";
		}
		this.servicoRemoto.persistirConta(conta, this.numeroConta);
		return "Retire seu dinheiro";
	}

}
