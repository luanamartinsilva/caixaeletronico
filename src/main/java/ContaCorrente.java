
public class ContaCorrente implements Cloneable {
	
	private Double saldo;

	public ContaCorrente(Double saldo) {
		this.saldo = saldo;
	}

	public void setSaldo(Double valor) {
		this.saldo = valor;
	}

	public Double getSaldo() {
		return this.saldo;
	}
	
	@Override
	public Object clone() {
	    ContaCorrente conta = null;
	    try {
	        conta = (ContaCorrente) super.clone();
	    } catch (CloneNotSupportedException e) {
	        conta = new ContaCorrente(this.getSaldo());
	    }
	    return conta;
	}

}
